module gitlab.com/xyko/gcsload

go 1.16

require (
	cloud.google.com/go/storage v1.15.0
	github.com/google/uuid v1.2.0
	google.golang.org/api v0.45.0
)
