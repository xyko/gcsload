.PHONY: all
all: gcsload

.PHONY: gcsload
gcsload:
	.scripts/bazel build gcsload

.PHONY: run
run: gcsload
	GOMAXPROCS=4 bazel-bin/gcsload_/gcsload

.PHONY: clean
clean:
	.scripts/bazel clean
