package main

import (
	"context"
	"io/ioutil"
	"log"
	"runtime"
	"sync"

	"cloud.google.com/go/storage"
	"github.com/google/uuid"
	"google.golang.org/api/option"
)

var hello = []byte("hello ")

func main() {
	const parallel = 64
	const filesPerClient = 10
	const fsize = 100 * 1024 // approximated file size

	ctx := context.Background()

	memStatus := runtime.MemStats{}
	logMemUsage := func() {
		runtime.ReadMemStats(&memStatus)
		log.Printf("sys: %f", float64(memStatus.Sys)/1024/1024)
	}

	logMemUsage()

	client, err := storage.NewClient(ctx, option.WithCredentialsJSON(mustLoadGcsKey()))
	if err != nil {
		log.Fatalf("error creating client: %v", err)
	}

	wg := &sync.WaitGroup{}
	for i := 0; i < filesPerClient; i++ {
		for j := 0; j < parallel; j++ {
			wg.Add(1)
			go func(i, j int) {
				defer wg.Done()

				writer := client.Bucket("k8sexp").Object("tmp/" + uuid.New().String()).NewWriter(ctx)
				writer.ChunkSize = 256 * 1024 // that's the minimum size
				defer func(i, j int) {
					if err := writer.Close(); err != nil {
						log.Printf("failed to close file %d/%d: %v", i, j, err)
					}
				}(i, j)

				for s := 0; s < fsize; s += len(hello) {
					_, err := writer.Write(hello)
					if err != nil {
						log.Printf("failed write to file %d/%d: %v", i, j, err)
						return
					}
				}
			}(i, j)

		}
		wg.Wait()

		logMemUsage()
	}
}

func mustLoadGcsKey() []byte {
	key, err := ioutil.ReadFile("key.json")
	if err != nil {
		log.Panicf("error gcs key from key.json: %v", err)
	}

	return key
}
