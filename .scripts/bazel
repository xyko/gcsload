#!/usr/bin/env bash
set -eo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
BAZELVER="$(cat "${SCRIPTPATH}/../.bazelversion")"

BAZEL_RFILE="bazel-${BAZELVER}-linux-x86_64"
BAZEL_RSHA="bazel-${BAZELVER}-linux-x86_64.sha256"

CACHEDIR="${HOME}/.cache/tagged-bazel"

function install_bazel() {
	if [ -f "${CACHEDIR}/${BAZEL_RFILE}" ]; then
		return
	fi

	# fetch and check on a temporary directory
	(
		mkdir -p "${CACHEDIR}"
		cd "${CACHEDIR}"
		echo "Fetching ${BAZEL_RFILE}"

		rm -rf "fetch-${BAZELVER}" && mkdir -p "fetch-${BAZELVER}" && cd "fetch-${BAZELVER}"

		curl --fail -sLo "${BAZEL_RFILE}" "https://github.com/bazelbuild/bazel/releases/download/${BAZELVER}/${BAZEL_RFILE}"
		curl --fail -sLo "${BAZEL_RSHA}"  "https://github.com/bazelbuild/bazel/releases/download/${BAZELVER}/${BAZEL_RSHA}"

		sha256sum -c "${BAZEL_RSHA}"

		chmod +x "${BAZEL_RFILE}"

		# move to the final directory
		rm -f    "${CACHEDIR}/${BAZEL_RFILE}"
		mv       "${BAZEL_RSHA}"  "${CACHEDIR}/${BAZEL_RSHA}"
		mv       "${BAZEL_RFILE}" "${CACHEDIR}/${BAZEL_RFILE}"
	)
}

# main
install_bazel

if [ "$1" == "build" ]; then
	shift

	CACHEP=build

	if [ -f ~/.bazel-gcs-key.json ]; then
		CACHEP+=" --remote_cache=https://storage.googleapis.com/sisubzlcache"
		CACHEP+=" --google_credentials=$(readlink -f ~/.bazel-gcs-key.json)"
		CACHEP+=" --experimental_guard_against_concurrent_changes"
	fi
fi

# sanitize environment variables
env -i PATH=/usr/local/bin:/usr/bin:/bin "${CACHEDIR}/${BAZEL_RFILE}" $CACHEP "$@"
